export const SizeSelector = ({shoe, select, quantity, size}) => {
	return (
		<>
			<h4 className="font-bold text-lg">Size</h4>
			<div className="btn-group">
				{
					Object.keys(shoe.availability).map(sizeChoice => (
						<input
							key={`${shoe.id}-${sizeChoice}`}
							type="radio"
							name="options"
							data-title={sizeChoice}
							className="btn"
							disabled={shoe.availability[sizeChoice] < quantity}
							onChange={() => select(sizeChoice)}
							checked={size === sizeChoice}
						/>
					))
				}
			</div>
		</>
	)
}