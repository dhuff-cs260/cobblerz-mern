import Question from "./Question";

const QuestionList = ({questionData}) => {
	const items = questionData.map((faq, index) => <Question key={index} faq={faq}/>);
	return (
		<div>{items}</div>
	)
};

export default QuestionList;