import {AddToCartModal} from "./AddToCartModal";

const ProductCard = ({product, addToCart}) => {
	return (
		<div className="flex-none card card-compact w-80 bg-base-100 h-[32rem] shadow-xl">
			<figure className="overflow-hidden w-80">
				<img src={product.imageURL} alt={product.title} />
			</figure>
			<div className="card-body">
				<h2 className="card-title">{product.title}</h2>
				<p>{product.description}</p>
				<p className="font-bold">${product.price}</p>
				<div className="card-actions justify-end">

					{/* THIS OPENS THE MODAL, BUT THE CART ADDING DOESN'T WORK YET */}
					<label htmlFor={`modal-${product.id}`} className="btn btn-primary">Add to Cart</label>

					{/* THIS IS THE OLD BUTTON */}
					{/*<button className="btn btn-primary" onClick={addToCart.bind(this, product)}>Add to Cart</button>*/}
				</div>
			</div>
			<AddToCartModal shoe={product} addToCart={addToCart}/>
		</div>
	);
}

export default ProductCard;