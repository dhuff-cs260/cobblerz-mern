import {HashLink as Link} from "react-router-hash-link";
import CartButton from "./CartButton";

const TopMenu = ({cartItems}) => {
	const closeDropdown = () => document.activeElement.blur();
	return (
		<div data-theme="forest" className="w-full navbar bg-base-300 sticky top-0 z-50">
			<div className="navbar-start">
				<label htmlFor="nav-drawer" className="btn btn-square btn-ghost md:hidden">
					<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-6 h-6 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
				</label>
				<Link to="/" className="btn btn-ghost normal-case text-xl hidden md:flex">Cobblerz</Link>
			</div>
			<div className="navbar-center">
				<Link to="/" className="btn btn-ghost normal-case text-xl md:hidden">Cobblerz</Link>
				<ul className="menu menu-horizontal p-0 hidden md:flex">
					<li><Link to="/#top">Home</Link></li>
					<li tabIndex={0}>
						<Link to="/shop#top">
							Shop
							<svg className="fill-current" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z"/></svg>
						</Link>
						<ul className="p-2 bg-base-300">
							<li onClick={closeDropdown}><Link to="/shop#Athletic">Athletic</Link></li>
							<li onClick={closeDropdown}><Link to="/shop#Lifestyle">Lifestyle</Link></li>
							<li onClick={closeDropdown}><Link to="/shop#Formal">Formal</Link></li>
						</ul>
					</li>
					<li tabIndex={1}>
						<Link to="/about#top">
							About Us
							<svg className="fill-current" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z"/></svg>
						</Link>
						<ul className="p-2 bg-base-300">
							<li onClick={closeDropdown}><Link to="/about#FAQ">FAQ</Link></li>
							<li onClick={closeDropdown}><Link to="/about#Contact">Contact</Link></li>
						</ul>
					</li>
				</ul>
			</div>
			<div className="navbar-end">
				<CartButton items={cartItems} />
			</div>
		</div>
	);
};

export default TopMenu;