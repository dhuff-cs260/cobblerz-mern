const Question = ({faq}) => {
	return (
        <>
        <div className="px-6 collapse collapse-arrow">
            <input type="checkbox" className="peer" /> 
            <div className="collapse-title border-base-300 border-b-2 bg-base-100 text-xl font-bold text-black peer-checked:border-black peer-checked:bg-base-100 peer-checked:text-black">
                {faq.question}
            </div>
            <div className="pt-2 collapse-content border-base-300 bg-base-100 peer-checked:bg-base-100 peer-checked:text-slate-400"> 
                <p className="text-slate-700">{faq.answer}</p>
            </div>
        </div>
        </>
	);
}

export default Question;