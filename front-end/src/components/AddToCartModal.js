import {SizeSelector} from "./SizeSelector";
import {useEffect, useState} from "react";

export const AddToCartModal = ({shoe, addToCart}) => {
	const [quantity, setQuantity] = useState(1);
	const [size, setSize] = useState(null);

	useEffect(() => {
		if (shoe.availability[size] < quantity) {
			setSize(null);
		}
	}, [quantity]);

	return (
		<>
			<input type="checkbox" id={`modal-${shoe.id}`} className="modal-toggle" />
			<div className="modal">
				<div className="modal-box w-11/12 max-w-3xl min-h-[320px]">
					<label htmlFor={`modal-${shoe.id}`} className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
					<label
						htmlFor={`modal-${shoe.id}`}
						className={["btn btn-primary absolute right-4 bottom-4", !size ? "btn-disabled" : ""].join(" ")}
						onClick={() => {
							setQuantity(1);
							setSize(null);
							addToCart(shoe, size, quantity);
						}}
					>
						Add to Cart
					</label>
					<div className="flex gap-x-4">
						<div className="basis-1/3">
							<img src={shoe.imageURL} alt={shoe.title} className="w-full rounded-xl"/>
						</div>
						<div className="basis-2/3">
							<h3 className="font-bold text-xl">{shoe.title}</h3>
							<h5 className="font-bold pb-2">${shoe.price}</h5>
							<h5 className="font-bold text-lg">Quantity</h5>
							<input
								type="number"
								value={quantity}
								onInput={(event) => setQuantity(parseInt(event.target.value))}
								className="input input-bordered w-full max-w-xs"
								min={1}
							/>
							<SizeSelector shoe={shoe} select={setSize} size={size} quantity={quantity}/>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}