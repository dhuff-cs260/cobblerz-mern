import axios from 'axios';

axios.defaults.withCredentials = true;

export const fetchShoes = async () => {
	try {
		return (await axios.get('/api/shoes')).data.shoes;
	} catch (e) {
		alert(e);
		return [];
	}
}

export const fetchFeaturedShoes = async () => {
	try {
		return (await axios.get('/api/shoes/featured')).data.shoes;
	} catch (e) {
		alert(e);
		return [];
	}
}

export const fetchShoesByCategory = async (category) => {
	try {
		return (await axios.get(`/api/shoes/category/${category}`)).data.shoes;
	} catch (e) {
		alert(e);
		return [];
	}
}

export const fetchFaq = async () => {
	try {
		return (await axios.get('/api/faq')).data.faqs;
	} catch (e) {
		alert(e);
		return [];
	}
}

// we don't need to access the shoe POST or DELETE endpoints directly from the front end

export const fetchCart = async () => {
	try {
		return (await axios.get('/api/cart')).data.items;
	} catch (e) {
		alert(e);
		return [];
	}
}

export const addToCart = async (shoeId, size, quantity) => {
	try {
		return (await axios.post(`/api/cart/${shoeId}`, {size, quantity})).data.item;
	} catch (e) {
		alert(e);
		return {};
	}
}

export const getProductFromCartItem = async (cartItem) => {
	try {
		return (await axios.get(`/api/shoes/id/${cartItem.productId}`)).data.shoe;
	} catch (e) {
		alert(e);
		return {};
	}
}

export const updateCartItem = async (id, size, quantity) => {
	try {
		return (await axios.put(`/api/cart/${id}`, {size, quantity})).data.item;
	} catch (e) {
		alert(e);
		return {};
	}
}

export const removeCartItem = async (id) => {
	try {
		return (await axios.delete(`/api/cart/${id}`)).data.success;
	} catch (e) {
		alert(e);
		return false;
	}
}

export const clearCart = async () => {
	try {
		return (await axios.delete('/api/cart')).data.success;
	} catch (e) {
		alert(e);
		return false;
	}
}