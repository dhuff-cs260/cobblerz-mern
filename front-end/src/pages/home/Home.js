import { useState, useEffect } from 'react';
import { fetchFeaturedShoes } from '../../net/ServerFacade';
import SectionHeader from "../../components/SectionHeader";
import ProductList from "../../components/ProductList";
import Footer from "../../components/Footer";
import {Hero} from "../../components/Hero";

const Home = ({addToCart}) => {
	const [featured, setFeatured] = useState([]);

	const fetchFeatured = async() => {
		try {      
		  setFeatured(await fetchFeaturedShoes());
		} catch(error) {
		  console.log("Error Fetching Featured Shoes")
		}
	}

	// fetch shoe data
	useEffect(() => {
		fetchFeatured().then();
	},[]);

	return (
		<>
			<div className="invisible" id="top"></div>
			<Hero
				style={{backgroundImage: 'url(' + require('../../assets/images/slide2.jpg') + ')'}}
				title="Cobblerz"
				bodyText="S-Tier cobbling services since 2022"
				actionText="Browse New Arrivals"
				actionLink="/#Featured"
			/>
			<SectionHeader title="Featured"/>
			<ProductList products={featured} addToCart={addToCart}/>
			<Footer/>
		</>
	);

};

export default Home;