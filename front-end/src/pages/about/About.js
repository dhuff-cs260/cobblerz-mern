import { useState, useEffect } from 'react';
import SectionHeader from "../../components/SectionHeader";
import QuestionList from "../../components/QuestionList";
import ContactForm from "../../components/ContactForm";
import Footer from "../../components/Footer";
import { fetchFaq } from '../../net/ServerFacade';

const About = () => {

	const [faq, setFAQ] = useState([]);

	const fetchFAQ= async() => {
		try {      
		  setFAQ(await fetchFaq());
		} catch(error) {
		  console.log("Error Fetching FAQ")
		}
	}

	// fetch faq data
	useEffect(() => {
		fetchFAQ();
	},[]);

	return (
		<>
			<div className="invisible" id="top"></div>
			<SectionHeader title="FAQ" />
			<QuestionList questionData={faq}/>
			<SectionHeader title="Contact" id="contact"/>
			<ContactForm/>
			<Footer/>
		</>
	);
};

export default About;