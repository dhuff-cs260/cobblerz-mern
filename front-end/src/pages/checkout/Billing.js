import Cleave from 'cleave.js/react';
import { useState} from 'react';

const Billing = () => {

    const [creditCardNo, setCreditCardNo] = useState("");
    const [creditCardExpiryDate, setCreditCardExpiryDate] = useState("");
    const [cvv, setCVV] = useState("");

    function onCreditCardChange(e) {
        setCreditCardNo(e.target.rawValue);
    }

    function onCreditCardExpiryChange(e) {
        setCreditCardExpiryDate(e.target.rawValue);
    }

    function onCVVChange(e) {
        setCVV(e.target.rawValue);
    }

    return (
        <>
            <form className="justify-center w-full max-w-3xl mx-auto" method="post">
                <div className="flex flex-col sm:flex-row justify-center">
                    <div className="form-control w-full max-w-s sm:pr-4">
                        <input type="text" placeholder="First Name" className="input input-bordered w-full max-w-s" />
                    </div>
                    <div className="pt-4"/>
                    <div className="form-control w-full">
                        <input type="text" placeholder="Last Name" className="input input-bordered w-full" />
                    </div>
                </div>
                <div className="form-control w-full">
                    <div className="pt-4"/>
                    <input type="email" placeholder="Email" className="input input-bordered w-full" />
                </div>
                <div className="form-control">
                    <div className="pt-4"/>
                    <div className="input-group">
                        <select required className="select select-bordered">
                            <option disabled>Country / Region</option>
                            <option >United States</option>
                            <option>Canada</option>
                            <option>France</option>
                            <option>Germany</option>
                            <option>Italy</option>
                            <option>Mexico</option>
                            <option>Philippines</option>
                            <option>United Kingdom</option>
                        </select>
                    </div>
                </div>
                <div className="form-control w-full">
                    <div className="pt-4"/>
                    <Cleave
                        creditcardno={creditCardNo}
                        placeholder="Credit Card Number"
                        options={{creditCard: true}}
                        onChange={onCreditCardChange}
                        className="form-field input input-bordered w-full"
                    />
                </div>
                <div className="flex flex-col sm:flex-row justify-center">
                    <div className="form-control w-full">
                        <div className="pt-4"/>
                        <input type="text" placeholder="Name on Card" className="input input-bordered w-full" />
                    </div>
                </div>
                <div className="flex flex-col sm:flex-row justify-center">
                    <div className="form-control w-full sm:pr-4">
                        <div className="pt-4"/>
                        <Cleave
                            creditcardexpirydate={creditCardExpiryDate}
                            placeholder="Expires MM/YY"
                            options={{ date: true, datePattern: ["m", "d"] }}
                            onChange={onCreditCardExpiryChange}
                            className="form-field input input-bordered w-full"
                        />
                    </div>
                    <div className="form-control w-full">
                        <div className="pt-4"/>
                        <Cleave
                            cvv={cvv}
                            placeholder="CVV"
                            options={{blocks: [3], numericOnly: true}}
                            onChange={onCVVChange}
                            className="form-field input input-bordered w-full"
                        />
                    </div>
                </div>
            </form>
        </>
    );
};

export default Billing;