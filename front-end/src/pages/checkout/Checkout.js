import SectionHeader from "../../components/SectionHeader";
import Footer from "../../components/Footer";
import Shipping from "./Shipping";
import Billing from "./Billing";
import CheckoutModal from "./CheckoutModal";
import {useState} from "react";

const Checkout = ({clear, cartItems}) => {

    const [fullInputs, setFullInputs] = useState(false);
    const [resetCounter, setResetCounter] = useState(0);

    const itemCount = cartItems.reduce((acc, item) => +(acc + item.quantity), 0);

    function clearFormAndCart(){
        let allInputsHaveValues = true;
        let inputs = Array.from(document.getElementsByClassName('input'));
        // Used to get the last input to not be in the list
        // Will need to fix later.
        inputs.pop();

        inputs.forEach(input => {
            if(input.value === ""){
                allInputsHaveValues = false;
            }
        })

        if(allInputsHaveValues && itemCount > 0){
            setFullInputs(true);
            setResetCounter(resetCounter + 1);
            clear();
        }
        else{
            setFullInputs(false);
        }
    }
    return (
        <>
            <div className="invisible" id="top"></div>
            <div className="flex flex-col md:w-full px-10">
                <SectionHeader title="Billing Information"/>
                <Billing key={`billing-${resetCounter}`}/>
                <SectionHeader title="Shipping Address"/>
                <Shipping key={`shipping-${resetCounter}`}/>
                <div className="pt-4"/>
                <label
                    htmlFor="my-modal-6"
                    onClick={clearFormAndCart}
                    className="btn btn-primary w-full max-w-3xl mx-auto"
                >
                    Complete Order
                </label>
                <div className="pt-4"/>
                <label
                    htmlFor="my-modal-6"
                    onClick={clearFormAndCart}
                    className="btn bg-gray-900 text-white flex flex-row justify-center items-center w-full max-w-3xl mx-auto"
                >
                    <div>
                        <svg className="fill-current" width="16" height="16">
                            <path
                                d="M10.9099 4.27692C9.6499 4.27692 9.1174 4.87817 8.2399 4.87817C7.34021 4.87817 6.65396 4.28129 5.56208 4.28129C4.49333 4.28129 3.35365 4.93379 2.6299 6.04535C1.61365 7.61285 1.78615 10.565 3.43208 13.08C4.02083 13.9804 4.80708 14.99 5.83833 15.001H5.85708C6.75333 15.001 7.01958 14.4141 8.25302 14.4072H8.27177C9.48677 14.4072 9.73052 14.9975 10.623 14.9975H10.6418C11.673 14.9866 12.5015 13.8679 13.0902 12.971C13.514 12.326 13.6715 12.0022 13.9965 11.2725C11.6155 10.3688 11.233 6.99348 13.5877 5.69942C12.869 4.79942 11.859 4.27817 10.9068 4.27817L10.9099 4.27692Z"
                                fill="currentColor"
                            />
                            <path d="M10.6338 1C9.88379 1.05094 9.00879 1.52844 8.49629 2.15188C8.03129 2.71688 7.64879 3.555 7.79879 4.36781H7.85879C8.65754 4.36781 9.47504 3.88688 9.95254 3.27063C10.4125 2.68406 10.7613 1.85281 10.6338 1V1Z" fill="currentColor" />
                        </svg>
                    </div>
                    <div>
                        <p className="text-base leading-4">Pay</p>
                    </div>
                </label>
            </div>
            <Footer/>
            <CheckoutModal fullInputs={fullInputs}/>
        </>
    );
};

export default Checkout;