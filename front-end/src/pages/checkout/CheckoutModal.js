const CheckoutModal = ({fullInputs}) => {

    if(fullInputs){
        return (
            <>
                <input type="checkbox" id="my-modal-6" className="modal-toggle" />
                <div className="modal modal-bottom sm:modal-middle">
                    <div className="modal-box">
                        <h3 className="font-bold text-lg">Thank you for your purchase!</h3>
                        <p className="py-4">An email with your order number and details will be sent shortly.</p>
                        <div className="modal-action">
                            <label htmlFor="my-modal-6" className="btn">Ok</label>
                        </div>
                    </div>
                </div>
            </>
        );
    }
    else{
        return (
            <>
                <input type="checkbox" id="my-modal-6" className="modal-toggle" />
                <div className="modal modal-bottom sm:modal-middle">
                    <div className="modal-box">
                        <h3 className="font-bold text-lg">Oh no!</h3>
                        <p className="py-4">It seems that you either forgot to fill out an input field, or you don't have any items in your cart.</p>
                        <div className="modal-action">
                            <label htmlFor="my-modal-6" className="btn">Ok</label>
                        </div>
                    </div>
                </div>
            </>
        );
    }
};

export default CheckoutModal;