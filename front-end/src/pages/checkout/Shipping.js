import Cleave from 'cleave.js/react';
import { useState} from 'react';
import"cleave.js/dist/addons/cleave-phone.ng";

const Shipping = () => {

    const [phoneNumber, setPhoneNumber] = useState("");

    function onPhoneChange(e) {
        setPhoneNumber(e.target.rawValue);
    }
    return (
        <>
            <form className="justify-center w-full max-w-3xl mx-auto" method="post">
                <div className="flex flex-col sm:flex-row justify-center">
                    <div className="form-control w-full sm:pr-4">
                        <div className="pt-4"/>
                        <input type="text" placeholder="First Name" className="input input-bordered w-full" />
                    </div>

                    <div className="form-control w-full m">
                        <div className="pt-4"/>
                        <input type="text" placeholder="Last Name" className="input input-bordered w-full" />
                    </div>
                </div>
                <div className="pt-4"/>
                <div className="input-group">
                    <select required defaultValue={'default'} className="select select-bordered">
                        <option selected disabled>Country / Region</option>
                        <option >United States</option>
                        <option>Canada</option>
                        <option>France</option>
                        <option>Germany</option>
                        <option>Italy</option>
                        <option>Mexico</option>
                        <option>Philippines</option>
                        <option>United Kingdom</option>
                    </select>
                </div>
                <div className="form-control w-full m">
                    <div className="pt-4"/>
                    <input type="text" placeholder="Street Address" className="input input-bordered w-full" />
                </div>
                <div className="form-control w-full m">
                    <div className="pt-4"/>
                    <input type="text" placeholder="Apartment, suite, etc. (optional)" className="input input-bordered w-full" />
                </div>
                <div className="flex flex-col sm:flex-row justify-center">
                    <div className="form-control w-full m sm:pr-4">
                        <div className="pt-4"/>
                        <input type="text" placeholder="City" className="input input-bordered w-full" />
                    </div>

                    <div className="form-control w-full m">
                        <div className="pt-4"/>
                        <input type="text" placeholder="ZIP Code" className="input input-bordered w-full" />
                    </div>
                </div>
                <div className="form-control w-full m">
                    <div className="pt-4"/>
                    <Cleave
                        phonenumber={phoneNumber}
                        placeholder="Cell Phone"
                        options={{ phone: true, phoneRegionCode:"US" }}
                        onChange={onPhoneChange}
                        className="form-field input input-bordered w-full"
                    />
                </div>
            </form>
        </>
    );
};

export default Shipping;