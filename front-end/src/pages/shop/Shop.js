import { useState, useEffect } from 'react';
import SectionHeader from "../../components/SectionHeader";
import ProductList from "../../components/ProductList";
import { fetchShoesByCategory } from '../../net/ServerFacade';
import Footer from "../../components/Footer";
import {Hero} from "../../components/Hero";

const Shop = ({addToCart}) => {

	const [athletic, setAthletic] = useState([]);
	const [lifestyle, setLifeStyle] = useState([]);
	const [formal, setFormal] = useState([]);

	const fetchAthletic = async() => {
		try {      
		  setAthletic(await fetchShoesByCategory("athletic"));
		} catch(error) {
		  console.log("Error Fetching Athletic Shoes")
		}
	}

	const fetchLifeStyle = async() => {
		try {      
		  setLifeStyle(await fetchShoesByCategory("lifestyle"));
		} catch(error) {
		  console.log("Error Fetching Lifestyle Shoes")
		}
	}

	const fetchFormal = async() => {
		try {      
		  setFormal(await fetchShoesByCategory("formal"));
		} catch(error) {
		  console.log("Error Fetching Formal Shoes")
		}
	}
	
	// fetch shoe data
	useEffect(() => {
		fetchAthletic().then();
		fetchLifeStyle().then();
		fetchFormal().then();
	},[]);
	return (
		<>
			<div className="invisible" id="top"></div>
			<Hero
				style={{backgroundImage: 'url(' + require('../../assets/images/slide1.jpg') + ')'}}
				title="Shop The Hottest Styles"
				bodyText="Come and check out some of the hottest styles of the season!"
				actionText="Get Started" actionLink="/shop#Athletic"
			/>
			<SectionHeader title="Athletic"/>
			<ProductList products={athletic} addToCart={addToCart}/>
			<SectionHeader title="Lifestyle"/>
			<ProductList products={lifestyle} addToCart={addToCart}/>
			<SectionHeader title="Formal"/>
			<ProductList products={formal} addToCart={addToCart}/>
			<Footer/>
		</>
	);
};

export default Shop;