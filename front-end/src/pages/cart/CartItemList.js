import CartItem from "./CartItem";
import { HashLink as Link } from "react-router-hash-link";

const CartItemList = ({items, removeFromCart}) => {
	const itemCards = items.map((item, index) => <CartItem key={index} item={item} removeFromCart={removeFromCart} />);
	if (itemCards.length) {
		return (
			<div className="w-full px-8 pb-8 flex flex-col gap-y-4">{itemCards}</div>
		);
	}
	else {
		return (
			<div className="w-full h-[60vh] flex flex-col items-center justify-center font-bold text-xl">
				<div className="text-xl pb-4">Looks like you haven't added anything yet...</div>
				<Link to="/shop#top"><button className="btn btn-primary">Return to Shop</button></Link>
			</div>
		);
	}
}

export default CartItemList;