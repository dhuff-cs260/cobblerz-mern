import {HashLink as Link} from "react-router-hash-link";

const CartItem = ({item, removeFromCart}) => {
	return (
		<div className="card card-side bg-base-100 shadow-xl">
			<figure><img className="w-48 sm:w-60 h-72 overflow-hidden" src={item.imageURL} alt={item.title}/></figure>
			<div className="card-body">
				<h2 className="card-title">{item.title}</h2>
				<p>{item.description}</p>
				<p className="font-bold">${item.price} x ({item.quantity}) = ${(item.price * item.quantity).toFixed(2)}</p>
				<p>Size: {item.size}</p>
				<p>Quantity: {item.quantity}</p>
				<div className="card-actions justify-center sm:justify-end">
					<button className="btn btn-accent w-[101.48px]" onClick={removeFromCart.bind(this, item)}>
						Remove
					</button>
					<Link to="/checkout#top">
						<label htmlFor={`item-checkout-${item.title}`} className="btn btn-primary w-[101.48px]">Buy Now</label>
					</Link>
					{/* <label htmlFor={`item-checkout-${item.title}`} className="btn btn-primary w-[101.48px]">Buy Now</label> */}
				</div>
			</div>
		</div>
	);
};

export default CartItem;