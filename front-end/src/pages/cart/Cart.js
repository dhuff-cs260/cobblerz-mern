import SectionHeader from "../../components/SectionHeader";
import CartItemList from "./CartItemList";
import Footer from "../../components/Footer";
import {HashLink as Link} from "react-router-hash-link";

const Cart = ({items, removeFromCart}) => {
	if (items.length) {
		return (
			<>
				<div className="invisible" id="top"></div>
				<SectionHeader title="My Cart"/>
				<Link to="/checkout#top" className="w-full flex justify-end px-8 pb-4">
					<button onClick={() => document.activeElement.blur()} className="btn btn-primary">Proceed to Checkout</button>
				</Link>
				<CartItemList items={items} removeFromCart={removeFromCart} />
				<Footer/>
				<input type="checkbox" id="my-modal-6" className="modal-toggle" />
			</>
		);
	}
	else {
		return (
			<>
				<div className="invisible" id="top"></div>
				<SectionHeader title="My Cart"/>
				<CartItemList items={items} removeFromCart={removeFromCart} />
				<Footer/>
			</>
		);
	}

};

export default Cart;