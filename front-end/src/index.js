import ReactDOM from "react-dom/client";
import {useEffect, useState} from "react";
import { HashRouter, Routes, Route } from "react-router-dom";
import Layout from "./components/navigation/Layout";
import Home from "./pages/home/Home";
import Shop from "./pages/shop/Shop";
import About from "./pages/about/About";
import Cart from "./pages/cart/Cart";
import Checkout from "./pages/checkout/Checkout";
import * as ServerFacade from "./net/ServerFacade";

export default function App() {
    const [cartItems, setCartItems] = useState([]);

    const fetchCartItems = async () => {
        try {
            let dbItems = (await ServerFacade.fetchCart());
            dbItems = await Promise.all(dbItems.map(async (item) => ({
                itemId: item.id,
                ...(await ServerFacade.getProductFromCartItem(item)),
                size: item.size,
                quantity: item.quantity
            })));
            setCartItems(dbItems);
        } catch (e) {
            console.log("Error Fetching Featured Shoes");
        }
    }

    useEffect(() => {fetchCartItems().then()}, []);

    const addToCart = async (item, size, quantity) => {
        const added = await ServerFacade.addToCart(item.id, size, quantity);
        const existing = cartItems.find(e => e.itemId === added.id);
        Object.assign(item, {itemId: added.id, size, quantity: existing ? existing.quantity + quantity : quantity});
        setCartItems([...cartItems.filter(existing => existing.itemId !== item.itemId), item])
    };
    const removeFromCart = async (item) => {
        await ServerFacade.removeCartItem(item.itemId);
        setCartItems([...cartItems].filter(cartItem => cartItem !== item))
    };
    const clearCart = async () => {
        const success = await ServerFacade.clearCart();
        setCartItems(success ? [] : [...cartItems]);
    }

    return (
        <HashRouter>
            <Routes>
                <Route path="/" element={<Layout cartItems={cartItems} />}>
                    <Route index element={<Home addToCart={addToCart} />} />
                    <Route path="shop" element={<Shop addToCart={addToCart} />} />
                    <Route path="about" element={<About />} />
                    <Route path="cart" element={<Cart items={cartItems} removeFromCart={removeFromCart}/>} />
                    <Route path="checkout" element={<Checkout cartItems={cartItems} clear={clearCart}/>}/>
                </Route>
            </Routes>
        </HashRouter>
    );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);