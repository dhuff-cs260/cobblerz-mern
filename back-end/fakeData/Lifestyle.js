const LifeStyle = [
		{
			title: "Blazer",
			featured: false,
			category: "lifestyle",
			description: "New spin on an old classic",
			price: 99.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/blazer.webp"
		},
		{
			title: "Morris Clown",
			featured: false,
			category: "lifestyle",
			description: "What's life without laughs",
			price: 79.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/clown.webp"
		},
		{
			title: "Drift",
			featured: true,
			category: "lifestyle",
			description: "Lift in drift",
			price: 89.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/drift.jpg"
		},
		{
			title: "Traktori",
			featured: true,
			category: "lifestyle",
			description: "Splashing through puddles never looked so stylin'",
			price: 69.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/traktori.jpeg"
		},
		{
			title: "Air Force 1",
			featured: false,
			category: "lifestyle",
			description: "Ultimate air, Ultimate style",
			price: 129.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/forces.jpg"
		},
		{
			title: "Air Jordan 1",
			featured: false,
			category: "lifestyle",
			description: "The ulitmate sneaker",
			price: 299.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/jordan1.webp"
		},
		{
			title: "Dunk Retro",
			featured: false,
			category: "lifestyle",
			description: "A return of the iconic retro style",
			price: 219.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/dunk.webp"
		},
		{
			title: "Yeezy 350",
			featured: false,
			category: "lifestyle",
			description: "Running never looked so good",
			price: 259.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/350.webp"
		}
	];

module.exports = LifeStyle;