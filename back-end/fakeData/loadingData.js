const axios = require("axios");

const athletic = require("./Athletic.js");
const formal = require("./Formal.js");
const lifestyle = require("./Lifestyle.js");
const FAQS = require("./faqData.js");

const baseURL = "http://localhost:3001";

athletic.forEach(async (product) => {
  const response = await axios.post(`${baseURL}/api/shoes`, product);
  if (response.status != 200)
    console.log(`Error adding ${product.name}, code ${response.status}`);
});

formal.forEach(async (product) => {
  const response = await axios.post(`${baseURL}/api/shoes`, product);
  if (response.status != 200)
    console.log(`Error adding ${product.name}, code ${response.status}`);
});

lifestyle.forEach(async (product) => {
  const response = await axios.post(`${baseURL}/api/shoes`, product);
  if (response.status != 200)
    console.log(`Error adding ${product.name}, code ${response.status}`);
});

FAQS.forEach(async (faq) => {
  const response = await axios.post(`${baseURL}/api/faq`, faq);
  if (response.status != 200)
    console.log(`Error adding ${faq.question}, code ${response.status}`);
});