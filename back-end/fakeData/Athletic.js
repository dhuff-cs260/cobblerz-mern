const Athletic = [
		{
			title: "Foam Runner",
			featured: false,
			category: "athletic",
			description: "The future of speed",
			price: 399.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/foam.jpeg"
		},
		{
			title: "Karst",
			featured: true,
			category: "athletic",
			description: "A real head turner",
			price: 105.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/karst.jpg"
		},
		{
			title: "Air Max",
			featured: true,
			category: "athletic",
			description: "Max air, max speed",
			price: 99.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/max.webp"
		},
		{
			title: "SKODUL",
			featured: false,
			category: "athletic",
			description: "Extra bounce",
			price: 89.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/skodiul.webp"
		},
		{
			title: "Champion",
			featured: false,
			category: "athletic",
			description: "There can only be one",
			price: 159.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/champ.jpeg"
		},
		{
			title: "990v5 Core",
			featured: false,
			category: "athletic",
			description: "The classic's never fail",
			price: 139.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/990.webp"
		},
		{
			title: "Barricade",
			featured: true,
			category: "athletic",
			description: "Unbreakable style",
			price: 129.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/barricade.webp"
		},
		{
			title: "Vapor Lite",
			featured: false,
			category: "athletic",
			description: "A new way to fly around the court",
			price: 105.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/vapor.jpg"
		}
	];

module.exports = Athletic;