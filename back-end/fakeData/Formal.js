const Formal = [
		{
			title: "Peu Touring",
			featured: false,
			category: "formal",
			description: "Ready for anywhere",
			price: 139.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/peu.jpg"
		},
		{
			title: "Croc Lifted",
			featured: false,
			category: "formal",
			description: "Groundbreaking twist on a classic style",
			price: 239.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/crocs.webp"
		},
		{
			title: "Space",
			featured: false,
			category: "formal",
			description: "Sleek, Mean, Beautiful",
			price: 339.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/space.webp"
		},
		{
			title: "Trooper",
			featured: false,
			category: "formal",
			description: "Winter is coming",
			price: 359.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/trooper.webp"
		},
		{
			title: "Tux Wing Tip",
			featured: false,
			category: "formal",
			description: "Every man needs a good tux",
			price: 279.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/tux.jpeg"
		},
		{
			title: "Classic Wing Tip",
			featured: false,
			category: "formal",
			description: "An unbeatable classic",
			price: 129.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/brown.jpg"
		},
		{
			title: "Jack Eden",
			featured: false,
			category: "formal",
			description: "High tops never looked so fine",
			price: 149.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/jack.webp"
		},
		{
			title: "Sonoma",
			featured: false,
			category: "formal",
			description: "Sleek new style on a classic shape",
			price: 139.99,
			availability: { "7": 3, "8": 5, "9": 10, "10": 12, "11": 8, "12": 10, "13": 11 },
			imageURL: "https://cobblerz.dallinhuff.com/media/Sonoma.webp"
		},
	];

module.exports = Formal;