const FAQData = [
		{
			question: "What methods of payment are accepted?",
			answer: "We accept payments by credit and debit card (Visa, Mastercard, American Express, and Discover) and Apple Pay. We do not accept pre-paid cards.",
        },
        {
			question: "How can I cancel my order?",
			answer: "Order cancellation requests should be made as soon as possible as some orders will ship same day. Once orders have been shipped we are unable to cancel.",
        },
        {
			question: "Do you restock items once they sell out?",
			answer: "In most cases we do not, but there are times when we will have a limited restock on select items. If you want updates on restock information we suggest you sign up for the Cobblerz mailing list.",
        },
        {
			question: "Can I make an exchange/return?",
			answer: "Cobblerz will gladly offer an item exchange/return in the form of store credit.",
        },
        {
			question: "I made a mistake on my order. Can I change the size or add items to my current order?",
			answer: "Unfortunately, we cannot make size changes or additions to existing orders. If a size change is needed please contact support@cobblerz.com with the order number and request a cancellation.",
        },
        {
			question: "I entered the wrong address. Can I change it for my order?",
			answer: "Our seller and buyer protection policy does not allow us to make address changes to orders. Please double-check your entered address before completing the order and checking out. If you have entered the wrong address please contact support@cobblerz.com with the order number and request a cancellation.",
        },
        {
			question: "Does Cobblerz accept refunds?",
			answer: "We have a strict no refund policy. No refunds are allowed unless due to product defect. All claims must be made within 5 days of delivery. Please contact support@cobblerz.com and provide an order number and detailed explanation of the issue. Claims are handled in a case-by-case basis.",
        },
	];

module.exports = FAQData;