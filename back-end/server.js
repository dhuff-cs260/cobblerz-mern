const express = require('express');
const cookieParser = require('cookie-parser');
const { randomUUID } = require('crypto');

const app = express();
app.use(express.json());
app.use(cookieParser());

const readCookie = (req) => {
	return req.cookies.clientId;
}

const writeCookie = (res, cookie) => {
	res.cookie(...cookie);
}

const generateCookie = (value) => {
	const cookieName = 'clientId';
	const cookieValue = value || randomUUID();
	const options = {
		maxAge: 1000 * 60 * 60 * 24 * 30, // expires after 30 days
		httpOnly: false,
		signed: false
	}
	return [cookieName, cookieValue, options]
}

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/cobblerz', {
	useUnifiedTopology: true,
	useNewUrlParser: true
});

const shoeSchema = new mongoose.Schema({
	title: String,
	price: Number,
	category: String,
	description: String,
	availability: { 7: Number, 8: Number, 9: Number, 10: Number, 11: Number, 12: Number, 13: Number },
	imageURL: String,
	featured: Boolean
});

shoeSchema.virtual('id')
	.get(function() {
		return this._id.toHexString();
	});

shoeSchema.set('toJSON', { virtuals: true });

const Shoe = mongoose.model('Shoe', shoeSchema);

const faqSchema = new mongoose.Schema({
	question: String,
	answer: String 
});

faqSchema.set('toJSON', { virtuals: true });	

const FAQ = mongoose.model('FAQ', faqSchema);

const cartItemSchema = new mongoose.Schema({
	clientId: String,
	productId: String,
	size: Number,
	quantity: Number
});

cartItemSchema.virtual('id')
	.get(function() {
		return this._id.toHexString();
	});

cartItemSchema.set('toJSON', { virtuals: true });

const CartItem = mongoose.model('CartItem', cartItemSchema);

const BASE_URL = '/api';
const SHOES_URL = `${BASE_URL}/shoes`;
const CART_URL = `${BASE_URL}/cart`;
const FAQ_URL = `${BASE_URL}/faq`;

/** SHOES METHODS */

/** get all shoes in the database */
app.get(SHOES_URL, async (req, res) => {
	try {
		let shoes = await Shoe.find();
		res.send({ success: true, shoes });
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

/** get a shoe by id from the database */
app.get(`${SHOES_URL}/id/:id`, async (req, res) => {
	const shoeId = req.params.id;
	try {
		let shoe = (await Shoe.find({ _id: shoeId }))[0];
		res.send({success: shoe.id === shoeId, shoe});
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

/** get featured shoes from the database */
app.get(`${SHOES_URL}/featured`, async (req, res) => {
	try {
		let featured = await Shoe.find({ featured: true });
		res.send({success: true, shoes: featured});
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

/** get all shoes with a certain category from the database */
app.get(`${SHOES_URL}/category/:category`, async (req, res) => {
	const category = req.params.category;
	try {
		let shoes = await Shoe.find({ category });
		res.send({ success: true, shoes });
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}

})

/** add a shoe to the database */
app.post(`${SHOES_URL}`, async (req, res) => {
	const shoe = new Shoe({
		title: req.body.title,
		price: req.body.price,
		category: req.body.category,
		description: req.body.description,
		availability: req.body.availability || { "7": 0, "8": 0, "9": 0, "10": 0, "11": 0, "12": 0, "13": 0 },
		imageURL: req.body.imageURL,
		featured: req.body.featured || false
	});

	try {
		const savedShoe = await shoe.save();
		res.send({ success: true, shoe: savedShoe });
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

// clear all shoes from the database
app.delete(`${SHOES_URL}`, async (req, res) => {
	try {
		await Shoe.deleteMany({});
		res.send({ success: true });
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

/** CART METHODS */

/** get all items in the cart */
app.get(CART_URL, async (req, res) => {
	const clientId = readCookie(req) ?? randomUUID();
	try {
		let cart = await CartItem.find({clientId});
		writeCookie(res, generateCookie(clientId));
		res.send({success: true, items: cart})
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

/** add a shoe to the cart */
app.post(`${CART_URL}/:id`, async (req, res) => {
	const clientId = readCookie(req) ?? randomUUID();
	const productId = req.params.id;
	const size = req.body.size;
	const quantity = req.body.quantity;
	try {
		let product = (await Shoe.find({_id: productId}))[0];
		if (product.availability[`${size}`] < quantity) {
			throw "Not available in that quantity";
		}
		const {title, price, category, description, imageURL, featured} = product;
		const replacement = {
			title, price, category, description, imageURL, featured,
			availability: { ...product.availability, [`${size}`]: product.availability[`${size}`] - quantity }
		};
		await Shoe.replaceOne({_id: productId}, replacement);
		let existing = (await CartItem.find({clientId, productId, size}))[0];
		const inserted = (existing)
			? await CartItem.replaceOne(
				{_id: existing.id},
				{clientId, productId, size, quantity: existing.quantity + quantity}
			)
			: await new CartItem({clientId, productId, size, quantity}).save();
		writeCookie(res, generateCookie(clientId));
		res.send({success: true, item: (await CartItem.find({clientId, productId, size}))[0]});
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

/** change the size and/or quantity of a shoe in the cart */
app.put(`${CART_URL}/:id`, async (req, res) => {
	const id = req.params.id;
	const size = req.body.size;
	const quantity = req.body.quantity;
	try {
		const item = (await CartItem.find({_id: id}))[0];
		if (!item) {
			throw "Can't update an item not in the cart";
		}
		let product = (await Shoe.find({_id: item.productId}))[0];
		if (product.availability[`${size}`] < quantity) {
			throw "Not available in that quantity";
		}
		const {title, price, category, description, imageURL, featured} = product;
		const updatedProduct = {
			title, price, category, description, imageURL, featured,
			availability: item.size === size
				? {...product.availability, [`${size}`]: product.availability[`${size}`] - quantity}
				: {
					...product.availability,
					[`${item.size}`]: product.availability[`${item.size}`] + item.quantity,
					[`${size}`]: product.availability[`${size}`] - quantity
				  }
		}
		await Shoe.replaceOne({_id: product.id}, updatedProduct);
		const {clientId, productId} = item;
		const updatedItem = {clientId, productId, size, quantity};
		await CartItem.replaceOne({_id: id}, updatedItem);
		res.send({success: true, item: updatedItem});
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

/** remove a shoe from the cart */
app.delete(`${CART_URL}/:id`, async (req, res) => {
	const itemId = req.params.id;
	try {
		const item = (await CartItem.find({_id: itemId}))[0];
		const product = (await Shoe.find({_id: item.productId}))[0];
		await Shoe.replaceOne({_id: product.id}, {
			title: product.title,
			category: product.category,
			description: product.description,
			price: product.price,
			imageURL: product.imageURL,
			availability: {
				...product.availability,
				[`${item.size}`]: product.availability[`${item.size}`] + item.quantity
			},
			featured: product.featured
		})
		await CartItem.deleteOne({_id: itemId});
		res.send({success: true});
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

/** clear all items from the cart for a given user */
app.delete(CART_URL, async (req, res) => {
	const clientId = readCookie(req) ?? randomUUID();
	try {
		await CartItem.deleteMany({clientId});
		writeCookie(res, generateCookie(clientId));
		res.send({success: true});
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
});

/** FAQ METHODS */

/** get all faq's from the database */
app.get(FAQ_URL, async(req, res) => {
	try {
		let faqs = await FAQ.find();
		res.send({ success: true, faqs });
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

/** add a faq to the database */
app.post(`${FAQ_URL}`, async (req, res) => {
	const faq = new FAQ({
		question: req.body.question,
		answer: req.body.answer
	});
	try {
		const savedFAQ = await faq.save();
		res.send({ success: true, faq: savedFAQ });
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

// clear all faqs from the database
app.delete(`${FAQ_URL}`, async (req, res) => {
	try {
		await FAQ.deleteMany({});
		res.send({ success: true });
	} catch (e) {
		console.log(e);
		res.sendStatus(500);
	}
})

app.listen(3001, () => console.log('listening on port: ' + 3001))

